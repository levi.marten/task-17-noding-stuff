// Imported node modules-------------
const fs = require('fs');
const readline = require('readline');
const operativeSystem = require('os');
const http = require('http');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
// --------------------------------------

// Constants defined --------------------------------------------------------------
// Message
const message = `
Choose an option:
1. Read package.json
2. Get my OS info
3. Start a HTTP server
-------------------------
Type a number, or 'q' to quit: `

// System info
const osInfo = `
SYSTEM MEMORY : ${(operativeSystem.totalmem() / 1024 / 1024 / 1024).toFixed(2)} GB
FREE MEMORY : ${(operativeSystem.freemem() / 1024 / 1024 / 1024).toFixed(2)} GB
CPU CORES : ${operativeSystem.cpus().length}
ARCH : ${operativeSystem.arch()}
PLATFORM : ${operativeSystem.platform()}
RELEASE : ${operativeSystem.release()}
USER : ${operativeSystem.userInfo().username}
`
// --------------------------------------------------------------------------------

// Options displayed and choices to be made ------------------------------------------
let loopTheQuestion = function () {
    rl.question(message, (answer) => {
        if (answer == 1) {
            fs.readFile(__dirname + '/package.json', 'utf-8', (err, contents) => {
                console.log('Reading package.json file \n' + contents);
                loopTheQuestion();
            });
        } else if (answer == 2) {
            console.log('\n ... \n\nGetting OS info...n\ ' + osInfo);
            loopTheQuestion();
        } else if (answer == 3) {
            http.createServer(function (req, res) {
                res.writeHead(200, { 'Content-Type': 'text/plain' });
                res.end('Hello Dewald...');
            }).listen(3000, 'localhost');
            console.log('\nStarting a HTTP server...')
            console.log('Listening on port 3000...')
            loopTheQuestion();
        } else if (answer == 'q') {
            rl.close();
        } else {
            console.log('Invalid option.');
            loopTheQuestion();
        }
    });
};

loopTheQuestion();
// End of the show